<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => '/'], function() use ($router) {
    $router->get('/', function() {
        return redirect('api');
    });

    $router->group(['prefix' => '/api'], function() use ($router) {
        $router->get('/', function() {
            return response()->json(['message' => 'Api Transactions PicPay', 'status' => 'Connected']);
        });

        $router->group(['prefix' => '/transactions'], function() use ($router) {
            $router->post('/', 'TransactionController@requestTransaction');
        });
    });
});
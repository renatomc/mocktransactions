<?php

namespace App\Exceptions;

class EqualUsersInTransactionException extends CustomException
{
    public function __construct($msg = 'Usuário payer_id e payee_id são iguais!', $code = 422)
    {
        parent::__construct($msg, $code);
    }
}

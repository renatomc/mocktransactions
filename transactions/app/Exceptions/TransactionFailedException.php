<?php

namespace App\Exceptions;

class TransactionFailedException extends CustomException
{
    public function __construct($msg = 'Transação não autorizada', $code = 401)
    {
        parent::__construct($msg, $code);
    }
}

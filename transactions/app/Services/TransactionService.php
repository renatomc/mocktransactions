<?php

namespace App\Services;

use App\Exceptions\TransactionFailedException;
use App\Helpers\ValidatorHelper;
use App\Validators\TransactionValidator;
use Illuminate\Http\Request;

class TransactionService
{
    public function requestTransaction(Request $request)
    {
        $requestPost = $request->post();

        $validator = (new TransactionValidator())->requestTransaction($requestPost);

        if ($validator->fails()) {
            throw new TransactionFailedException(
                ValidatorHelper::arrayToStringMessage(
                    $validator
                        ->errors()
                        ->messages()
                )
            );
        }

        

        return [
            'code' => 200,
            'message' => 'Transação autorizada com sucesso!'
        ];
    }
}

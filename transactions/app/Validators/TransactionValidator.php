<?php

namespace App\Validators;

use App\Exceptions\EqualUsersInTransactionException;
use App\Exceptions\TransactionFailedException;
use Illuminate\Support\Facades\Validator;

class TransactionValidator
{
    public function requestTransaction(array $request)
    {
        if (
            isset($request['payee_id']) && isset($request['payer_id'])
            && ($request['payee_id'] == $request['payer_id'])
        ) {
            throw new EqualUsersInTransactionException();
        }

        if (isset($request['value']) && ($request['value'] <= 0 || $request['value'] >= 100)) {
            throw new TransactionFailedException();
        }

        return Validator::make(
            $request,
            [
                'payee_id' => [
                    'required'
                ],
                'payer_id' => [
                    'required'
                ],
                'value' => [
                    'required',
                    'numeric'
                ]
            ],
            [
                'required' => 'O campo :attribute é obrigatório'
            ]
        );
    }
}

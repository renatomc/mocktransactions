<?php

namespace App\Helpers;

class ValidatorHelper
{
    public static function arrayToStringMessage(array $arrayMessage)
    {
        $strMessage = '';
        $arrKeys = array_keys($arrayMessage);
        foreach ($arrayMessage as $k => $v) {
            if (end($arrKeys) != $k) {
                $strMessage .= $v[0] . ' - ';
            } else {
                $strMessage .= $v[0];
            }
        }

        return $strMessage;
    }
}

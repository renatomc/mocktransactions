<?php

namespace App\Http\Controllers;

use App\Services\TransactionService;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function requestTransaction(Request $request, TransactionService $service)
    {
        return response()->json(
            $service->requestTransaction($request),
            200
        );
    }
}
